# Wireless Long Range Camera System (WLRCS)



# Set up

There are two sides to set up to ensure there is full duplex communcation between the client device and the server device

## Client Setup

The client code can be found in the src directory titled "client.cpp". If changes are made, and the file needs to be rebuilt, run ```make``` in that same directory. This will put the executable in wlrcs/bin/. To run this executable, run:
```
sudo ./../bin/clientLib
```
If you have a terminal set up, you will be able to see some text being displayed. A red light indicates that something went wrong.

To run this in the real world. The line to run the program must be added to the file /etc/rc.local. This is the file that runs any programs at boot.

### Dependencies
- TensorFlow Lite https://qengineering.eu/install-tensorflow-2-lite-on-raspberry-64-os.html
- OpenCV4 https://qengineering.eu/install-opencv-on-raspberry-64-os.html
- WiringPi https://github.com/WiringPi/WiringPi/tree/master

## Server Setup

The server setup is much easier. Make sure the gateway is plugged in and being powered. A blue ethernet cable must be connected to the device (PC or Desktop) runnning the commands. The gateway's ip address will automatically be set to 192.168.2.1. Now, all you need to do is run ```python3 receiver.py <EMAIL>``` in a command terminal assuming python3 is already installed. "EMAIL" must be replaced with an email that you want to send and receive messages from. NOTE: If you are using a gmail account, you must create an application password to bypass the 2-step verification. Once you see a message in the terminal saying that you connected to the gateway, everything should be good and running on the server side. 
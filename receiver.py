import paramiko
import os
import json
import base64
import smtplib, ssl
import sys

smtp_server = "smtp.gmail.com"
port = 465

# def send_message(phone_number, carrier, message):
#     recipient = phone_number + CARRIERS[carrier]
#     auth = (EMAIL, PASSWORD)
 
#     server = smtplib.SMTP("smtp.gmail.com", 587)
#     server.starttls()
#     server.login(auth[0], auth[1])
 
#     server.sendmail(auth[0], recipient, message)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(f"Usage: python3 {sys.argv[0]} <EMAIL>")
        sys.exit(0)
    EMAIL = sys.argv[1]
    PASSWORD = input("Type your password and press enter: ")
    context = ssl.create_default_context()

    #Creates an ssh connection with the gateway
    p = paramiko.SSHClient()
    p.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    p.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
    p.connect(hostname="192.168.2.1", username="ara", password="Ara@000000")
    stdin, stdout, stderr = p.exec_command("cd /var/config")
    text_data = ""
    sent = 0
    print("---------Connected to the Gateway---------")

    while True:
        #subscribes to the uplink for 5 seconds.
        stdin, stdout, stderr = p.exec_command("mosquitto_sub -t lora/+/up -W 5")
        opt = stdout.readlines()
        opt = "".join(opt)
        #if no data do nothing
        if opt != "":
            x = json.loads(opt)
            #The data is in b64 so we must decode that to ASCII
            data = base64.b64decode(x["data"]).decode("utf-8")
            print(data)

            if "None" in data:
                stdin, stdout, stderr = p.exec_command("mosquitto_pub -t lora/00:80:00:00:04:01:76:62/down -m '{ \"data\":\"Uw==\" }'") #Sends the character S to slow things down
                text_data = """Subject: FOUND NOTHING\n\nGot nothing new for you..."""
                with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
                    server.login(EMAIL, PASSWORD)
                    server.sendmail(EMAIL,EMAIL, text_data)
                    server.close()
            #if the important stuff is found then send a message
            elif ("person" in data) or ("truck" in data):
                text_data = "Subject: FOUND SOMETHING\n\nHey User!\nJust notifying you that the following was detected:\n" + data + "\nI hope this helps"
                #send_message(phone_number, carrier, text_data)
                #sent = 1 #set this to one so that we do not send any new messages
                with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
                    server.login(EMAIL, PASSWORD)
                    server.sendmail(EMAIL,EMAIL, text_data)
                    server.close()
                stdin, stdout, stderr = p.exec_command("mosquitto_pub -t lora/00:80:00:00:04:01:76:62/down -m '{ \"data\":\"Rg==\" }'") #Sends the character F to speed things up
#include <csignal>
#include <iostream>
#include <pigpio.h>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/ocl.hpp>
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/string_util.h"
#include "tensorflow/lite/model.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>


#define SERVO_MIN_THRESH 800
#define SERVO_MAX_THRESH 2200
#define SERVO_MID ((SERVO_MAX_THRESH + SERVO_MIN_THRESH) / 2)

#define WAIT_TIME 10000.0
#define FAST (WAIT_TIME/2)
#define SLOW ((WAIT_TIME) * (2))

using namespace std;
using namespace cv;

const int RedLed = 17;
const int YelLed = 27;
const int GreLed = 22;

const int servoBase = 13;
const int servoTop = 12;
const int buttonUp = 14;
const int buttonDown = 23;
const int buttonRight = 15;
const int buttonLeft = 18;

const size_t width = 300;
const size_t height = 300;

std::vector<std::string> Labels;
std::unique_ptr<tflite::Interpreter> interpreter;

volatile sig_atomic_t signal_received = 0;

void sigint_handler(int signal) {
	signal_received = signal;
}

/**
 * @brief Get the File Content of a .txt file containing the labels of objects we may detect
 * 
 * @param fileName that contains the labels that will be detected
 * @return true if successfully parsed file
 * @return false if unsuccessfully parsed file
 */
static bool getFileContent(std::string fileName)
{

	// Open the File
	std::ifstream in(fileName.c_str());
	// Check if object is valid
	if(!in.is_open()) return false;

	std::string str;
	// Read the next line from File untill it reaches the end.
	while (std::getline(in, str))
	{
		// Line contains string of length > 0 then save it in vector
		if(str.size()>0) Labels.push_back(str);
	}
	// Close The File
	in.close();
	return true;
}

/**
 * @brief writes the Red LED high and the others low
 * 
 */
void conError(void){
	gpioWrite(RedLed, PI_HIGH);
	gpioWrite(YelLed, PI_LOW);
	gpioWrite(GreLed, PI_LOW);
}

/**
 * @brief writes the Yellow LED high and the others low
 * 
 */
void waiting(void){
	gpioWrite(RedLed, PI_LOW);
	gpioWrite(YelLed, PI_HIGH);
	gpioWrite(GreLed, PI_LOW);
}

/**
 * @brief writes the Green LED high and the others low
 * 
 */
void good(void) {
	gpioWrite(RedLed, PI_LOW);
	gpioWrite(YelLed, PI_LOW);
	gpioWrite(GreLed, PI_HIGH);
}

/**
 * @brief This function checks the status of joining the network or sending a message
 * 
 * @param serial_port is the port that the xDOT is connected to
 * @return int that represents the status of the action
 */
int checkStatus(int serial_port){
	char buf[300];
	int status = 0;
	int i = 0;
	waiting();
	//these next two lines are to flush the tx terminal so that we are only looking at receive messages
	while(!serialDataAvail(serial_port)){}
	serialFlush(serial_port);
	while(!serialDataAvail(serial_port)){} //basically wait until data is available
	while(serialDataAvail(serial_port) || (status == 0)){
		while(!serialDataAvail(serial_port)){}
		buf[i] = serialGetchar(serial_port);
		if(buf[i] == 'R'){
			conError();
			status = -1;
			break;
		} else if(buf[i] == 'O') {
			good();
			status = 1;
			break;
		}
		i++;
	}
	serialFlush(serial_port);
	return status;
}

/**
 * @brief Looks at an image and tries to detect different objects
 * 
 * @param src - image that is a matrix
 * 
 * NOTE: This function is based on tuturial by Qengineering.com
 */
vector<string> detectFromImage(Mat &src) //may add another parameter containing a list of objects we may have found
{
    Mat image;
    vector<string> objects;
    int cam_width =src.cols;
    int cam_height=src.rows;

    // copy image to input as input tensor
    cv::resize(src, image, Size(300,300));
    memcpy(interpreter->typed_input_tensor<uchar>(0), image.data, image.total() * image.elemSize());

    interpreter->SetAllowFp16PrecisionForFp32(true);
    interpreter->SetNumThreads(4);      //quad core

//        cout << "tensors size: " << interpreter->tensors_size() << "\n";
//        cout << "nodes size: " << interpreter->nodes_size() << "\n";
//        cout << "inputs: " << interpreter->inputs().size() << "\n";
//        cout << "input(0) name: " << interpreter->GetInputName(0) << "\n";
//        cout << "outputs: " << interpreter->outputs().size() << "\n";

    interpreter->Invoke();      // run your model

    const float* detection_locations = interpreter->tensor(interpreter->outputs()[0])->data.f;
    const float* detection_classes=interpreter->tensor(interpreter->outputs()[1])->data.f;
    const float* detection_scores = interpreter->tensor(interpreter->outputs()[2])->data.f;
    const int    num_detections = *interpreter->tensor(interpreter->outputs()[3])->data.f;

    //there are ALWAYS 10 detections no matter how many objects are detectable
//        cout << "number of detections: " << num_detections << "\n";

    const float confidence_threshold = 0.5;
    for(int i = 0; i < num_detections; i++){
        if(detection_scores[i] > confidence_threshold){
            int  det_index = (int)detection_classes[i]+1;
            float y1=detection_locations[4*i  ]*cam_height;
            float x1=detection_locations[4*i+1]*cam_width;
            float y2=detection_locations[4*i+2]*cam_height;
            float x2=detection_locations[4*i+3]*cam_width;

            Rect rec((int)x1, (int)y1, (int)(x2 - x1), (int)(y2 - y1));
            rectangle(src,rec, Scalar(0, 0, 255), 1, 8, 0);
            putText(src, format("%s", Labels[det_index].c_str()), Point(x1, y1-5) ,FONT_HERSHEY_SIMPLEX,0.5, Scalar(0, 0, 255), 1, 8, 0);
        
            objects.push_back(Labels[det_index].c_str());
        }
    }

    // for(int i = 0; i < objects.size(); i++){
    //     cout << objects[i] << endl;
    // }

    return objects;
}

int main(int argc, char** argv){
	int serial_port;
    chrono::steady_clock::time_point Tbegin, Tend;
	char buf[10];
    float speed = WAIT_TIME;
	int status = 0;
    Mat frame;
    float f;
    int basePosition = SERVO_MID;
    int topPosition = SERVO_MID;
    bool received = false;

    // Load model
    std::unique_ptr<tflite::FlatBufferModel> model = tflite::FlatBufferModel::BuildFromFile("detect.tflite");

    // Build the interpreter
    tflite::ops::builtin::BuiltinOpResolver resolver;
    tflite::InterpreterBuilder(*model.get(), resolver)(&interpreter);

    interpreter->AllocateTensors();

    // Get the names
	bool result = getFileContent("COCO_labels.txt");
	if(!result)
	{
        cout << "loading labels failed" << endl;
        return -1;
	}

	if(gpioInitialise() == PI_INIT_FAILED){
		cout << "ERROR: Failed to initialize the GPIO interface." << endl;
		return -1;
	}
	if((serial_port = serialOpen("/dev/ttyACM0", 115200)) < 0) {//open serial port to the mutlitech client
		cout << "ERROR: Unable to open serial device" << endl;
		return -1;
	}
	gpioSetMode(RedLed, PI_OUTPUT);
	gpioSetMode(YelLed, PI_OUTPUT);
	gpioSetMode(GreLed, PI_OUTPUT);
	gpioSetMode(buttonUp, PI_INPUT);
	gpioSetMode(buttonDown, PI_INPUT);
	gpioSetMode(buttonRight, PI_INPUT);
	gpioSetMode(buttonLeft, PI_INPUT);
	gpioSetMode(servoBase, PI_ALT0);
	gpioSetMode(servoTop, PI_ALT0);
	gpioServo(servoTop, topPosition);
	gpioServo(servoBase, basePosition);
	waiting();
	cout << "connecting to gateway" << endl;
	//join the gateway network
	serialPrintf(serial_port, "at+join\r");
	status = checkStatus(serial_port);
	if(status == 1) cout << "CONNECTED!" << endl;
	else if(status == -1) {
        cerr << "UNABLE TO CONNECT!" << endl;
        return -1;
    }
	signal(SIGINT,sigint_handler);
	cout << "Press ctrl-c to exit." << endl;
	Tbegin = chrono::steady_clock::now();
    while(!signal_received){
        
        Tend = chrono::steady_clock::now();
        f = chrono::duration_cast <chrono::milliseconds> (Tend - Tbegin).count();
	
        serialPrintf(serial_port, "at+recv\r");
		while(!serialDataAvail(serial_port)){}
			while(serialDataAvail(serial_port) && !received){
	    	//cout << "here sir" << endl;
	    	while(!serialDataAvail(serial_port)){}
	    	//cout << "hi" << endl;
	    	buf[0] = serialGetchar(serial_port);
        	if(buf[0] == '4') {
        	    cout << "going faster" << endl;
        	    speed = FAST;
				received = true;
        	    break;
        	}
        	else if(buf[0] == '5') {
                cout << "going slower" << endl;
                speed = SLOW;
				received = true;
            	break;
        	}
	    	else {
				speed = WAIT_TIME;
	    	}
            
        }
		serialFlush(serial_port);

        if(f > speed){
            cout << speed << endl;
	    	received = false;
            system("rpicam-jpeg -o image.jpg -t 1"); //Takes a picture

            frame=imread("image.jpg");  //need to refresh frame before dnn class detection
            resize(frame, frame, Size(640,480));
            if (frame.empty()) {
                cerr << "ERROR: Unable to grab from the camera" << endl;
                break;
            }
            vector<string> objects = detectFromImage(frame);
            if(objects.size()){
               serialPrintf(serial_port, "at+send=");
               for(int i = 0; (i < 3) && (i < objects.size()); i++){
                  serialPrintf(serial_port, "%s,", objects[i].c_str());
               }
               serialPrintf(serial_port, "\r"); 
            }
            else serialPrintf(serial_port, "at+send=None\r");
            status = checkStatus(serial_port);

            //show output 
            // imshow("Frame", frame); //This is only used for debugging purposes

            // char esc = waitKey(5);
            // if(esc == 27) break;

            Tbegin = chrono::steady_clock::now();
        }

        if(gpioRead(buttonDown)){
			//std::cout << "down" << std::endl;
			topPosition++;
			topPosition = std::min(SERVO_MAX_THRESH, topPosition);
			gpioServo(servoTop, topPosition);
			usleep(1000);
		}
		else if(gpioRead(buttonUp)){
			//std::cout << "up" << std::endl;
			topPosition--;
			topPosition = std::max(SERVO_MIN_THRESH, topPosition);
			gpioServo(servoTop, topPosition);
			usleep(1000);
		}
		else if(gpioRead(buttonRight)){
			//std::cout << "right" << std::endl;
			basePosition--;
			basePosition = std::max(SERVO_MIN_THRESH, basePosition);
			gpioServo(servoBase, basePosition);
			usleep(1000);
		}
		else if(gpioRead(buttonLeft)){
			//std::cout << "left" << std::endl;
			basePosition++;
			basePosition = std::min(SERVO_MAX_THRESH, basePosition);
			gpioServo(servoBase, basePosition);
			usleep(1000);
		}
    }
	//quitting therefore cleanup
	gpioSetMode(RedLed, PI_INPUT);
	gpioSetMode(YelLed, PI_INPUT);
	gpioSetMode(GreLed, PI_INPUT);
	gpioSetMode(servoBase, PI_INPUT);
	gpioSetMode(servoTop, PI_INPUT);
	serialClose(serial_port);
	gpioTerminate();
    destroyAllWindows();
	return 0;
}

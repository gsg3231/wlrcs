import paramiko
import os
import json
import base64
import cv2
import numpy as np

'''This file is used to create an ssh connection and test receiving a 16x16 image in string hex array format'''

#Creates an ssh connection with the gateway
p = paramiko.SSHClient()
p.set_missing_host_key_policy(paramiko.AutoAddPolicy())
p.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
p.connect(hostname="192.168.2.1", username="ara", password="Ara@000000")
stdin, stdout, stderr = p.exec_command("cd /var/config")

text_data = ""
img = np.zeros((16,16,3), dtype = "uint8")
print("---------Connected to the Gateway---------")

while True:
    #subscribes to the uplink for 5 seconds.
    stdin, stdout, stderr = p.exec_command("mosquitto_sub -t lora/+/up -W 2")
    opt = stdout.readlines()
    opt = "".join(opt)
    #if no data do nothing
    if opt != "":
        x = json.loads(opt)
        #The data is in b64 so we must decode that to ASCII
        data = base64.b64decode(x["data"]).decode("utf-8")
        print(data)

        #This is for a proof of concept of sending a 16x16 bgr image from client to gateway
        if(data != "EOF"):
            text_data = text_data + data
        else:
            i = 0
            print(text_data)
            for y in range(16):
                for x in range(16):
                    img[y][x][0] = int(text_data[i:i+2], 16)
                    i = i + 2
                    img[y][x][1] = int(text_data[i:i+2], 16)
                    i = i + 2
                    img[y][x][2] = int(text_data[i:i+2], 16)
                    i = i + 2
            cv2.namedWindow("Test")
            cv2.imshow("Test", img)
            cv2.waitKey(0)
            break
cv2.imwrite("./final.png", img)
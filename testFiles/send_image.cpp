#include <csignal>
#include <iostream>
#include <pigpio.h>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <opencv2/opencv.hpp>
#include <iomanip>
#include <sstream>
#include <string>
#include <stdlib.h>

/*Advanced code from serial_test.cpp*/

const int RedLed = 17;
const int YelLed = 27;
const int GreLed = 22;
const int Button = 2;
volatile sig_atomic_t signal_received = 0;

void sigint_handler(int signal) {
	signal_received = signal;
}

/**
 * @brief writes the Red LED high and the others low
 * 
 */
void conError(void){
	gpioWrite(RedLed, PI_HIGH);
	gpioWrite(YelLed, PI_LOW);
	gpioWrite(GreLed, PI_LOW);
}

/**
 * @brief writes the Yellow LED high and the others low
 * 
 */
void waiting(void){
	gpioWrite(RedLed, PI_LOW);
	gpioWrite(YelLed, PI_HIGH);
	gpioWrite(GreLed, PI_LOW);
}

/**
 * @brief writes the Green LED high and the others low
 * 
 */
void good(void) {
	gpioWrite(RedLed, PI_LOW);
	gpioWrite(YelLed, PI_LOW);
	gpioWrite(GreLed, PI_HIGH);
}

/**
 * @brief Converts a byte array to a hex string
 * 
 * @param data a byte matrix
 * @return std::string string of data in hex form
 */
std::string hexStr(cv::Mat data){
	std::stringstream ss;
	ss << std::hex;
	for(int y = 0; y < data.rows; y++){
		for(int x = 0; x < data.cols;  x++){
			ss << std::setw(2) << std::setfill('0') << (int) data.at<cv::Vec3b>(y,x)[0]; //b
			ss << std::setw(2) << std::setfill('0') << (int) data.at<cv::Vec3b>(y,x)[1]; //g
			ss << std::setw(2) << std::setfill('0') << (int) data.at<cv::Vec3b>(y,x)[2]; //r
		}
	}
	return ss.str();
}

/**
 * @brief This function checks the status of joining the network or sending a message
 * 
 * @param serial_port is the port that the xDOT is connected to
 * @return int that represents the status of the action
 */
int checkStatus(int serial_port){
	char buf[300];
	int status = 0;
	int i = 0;
	waiting();
	//these next two lines are to flush the tx terminal so that we are only looking at receive messages
	while(!serialDataAvail(serial_port)){}
	serialFlush(serial_port);
	while(!serialDataAvail(serial_port)){} //basically wait until data is available
	while(serialDataAvail(serial_port) || (status == 0)){
		while(!serialDataAvail(serial_port)){}
		buf[i] = serialGetchar(serial_port);
		if(buf[i] == 'R'){
			conError();
			status = -1;
			break;
		} else if(buf[i] == 'O') {
			good();
			status = 1;
			break;
		}
		i++;
	}
	serialFlush(serial_port);
	return status;
}

int main(int argc, char** argv){
	int serial_port;
	bool pressed = false;
	int counter = 0;
	int i;
	std::vector<uchar> buf;
	int status = 0;
//	cv::Mat img = cv::imread("Sprite_heart.png", cv::IMREAD_COLOR);
//	cv::Mat img = cv::imread("man.png", cv::IMREAD_COLOR);
	cv::Mat img = cv::imread(argv[1], cv::IMREAD_COLOR);
	if(img.empty()) return -1;
	if(gpioInitialise() == PI_INIT_FAILED){
		std::cout << "ERROR: Failed to initialize the GPIO interface." << std::endl;
		return -1;
	}
	if((serial_port = serialOpen("/dev/ttyACM0", 115200)) < 0) {//open serial port to the mutlitech client
		std::cout << "ERROR: Unable to open serial device" << std::endl;
		return -1;
	}
	gpioSetMode(Button, PI_INPUT);
	gpioSetMode(RedLed, PI_OUTPUT);
	gpioSetMode(YelLed, PI_OUTPUT);
	gpioSetMode(GreLed, PI_OUTPUT);
	waiting();
	std::cout << "connecting to gateway" << std::endl;
	//join the gateway network
	serialPrintf(serial_port, "at+join\r");
	status = checkStatus(serial_port);
	if(status == 1) std::cout << "CONNECTED!" << std::endl;
	else if(status == -1) std::cout << "UNABLE TO CONNECT!" << std::endl;
	signal(SIGINT,sigint_handler);
	std::cout << "Press ctrl-c to exit." << std::endl;
	while(!signal_received){
		//send a message and increment counter
		if(!gpioRead(Button) && !pressed){
			waiting();
			pressed = true;
			std::cout << "Sending image" << std::endl;
			std::string msg = hexStr(img);
			//Send the image by breaking it into packets of 242 bytes long
			counter = 0;
			while(msg[counter] != '\0'){
				serialPrintf(serial_port, "at+send=");
				for(i = 0; i < 242; i++){
					if(msg[counter + i] == '\0') break;
					serialPrintf(serial_port, "%c", msg[counter + i]);
				}
				serialPrintf(serial_port, "\r");
				status = checkStatus(serial_port);
				if(status == 1) counter += i; //retry if failed
				sleep(3);
			}
			//No more packets to send so send "EOF" packet
			serialPrintf(serial_port, "at+send=EOF\r");
			status = checkStatus(serial_port);
		} else if(gpioRead(Button)){
			pressed = 0;
		}
	}
	//quitting therefore cleanup
	gpioSetMode(RedLed, PI_INPUT);
	gpioSetMode(YelLed, PI_INPUT);
	gpioSetMode(GreLed, PI_INPUT);
	serialClose(serial_port);
	gpioTerminate();
	return 0;
}

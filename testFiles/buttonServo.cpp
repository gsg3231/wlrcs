#include <csignal>
#include <iostream>
#include <pigpio.h>
#include <unistd.h>

#define SERVO_MIN_THRESH 800
#define SERVO_MAX_THRESH 2200


const int servoBase = 13;
const int servoTop = 12;
const int buttonUp = 14;
const int buttonDown = 23;
const int buttonRight = 15;
const int buttonLeft = 18;
volatile sig_atomic_t signal_received = 0;

void sigint_handler(int signal) {
	signal_received = signal;
}

int main(void) {
	if(gpioInitialise() == PI_INIT_FAILED) {
		std::cout << "ERROR: Failed to initialise the GPIO interface" << std::endl;
		return -1;
	}
	
	int basePosition = 1500;
	int topPosition = 1500;
	gpioSetMode(buttonUp, PI_INPUT);
	gpioSetMode(buttonDown, PI_INPUT);
	gpioSetMode(buttonRight, PI_INPUT);
	gpioSetMode(buttonLeft, PI_INPUT);
	gpioSetMode(servoBase, PI_ALT0);
	gpioSetMode(servoTop, PI_ALT0);
	signal(SIGINT, sigint_handler);
	
	std::cout << "Press CTRL-C to exit." << std::endl;
	while(!signal_received) {
		if(gpioRead(buttonDown)){
			//std::cout << "down" << std::endl;
			topPosition++;
			topPosition = std::min(SERVO_MAX_THRESH, topPosition);
			gpioServo(servoTop, topPosition);
			usleep(1000);
		}
		else if(gpioRead(buttonUp)){
			//std::cout << "up" << std::endl;
			topPosition--;
			topPosition = std::max(SERVO_MIN_THRESH, topPosition);
			gpioServo(servoTop, topPosition);
			usleep(1000);
		}
		else if(gpioRead(buttonRight)){
			//std::cout << "right" << std::endl;
			basePosition--;
			basePosition = std::max(SERVO_MIN_THRESH, basePosition);
			gpioServo(servoBase, basePosition);
			usleep(1000);
		}
		else if(gpioRead(buttonLeft)){
			//std::cout << "left" << std::endl;
			basePosition++;
			basePosition = std::min(SERVO_MAX_THRESH, basePosition);
			gpioServo(servoBase, basePosition);
			usleep(1000);
		}
	}

	gpioSetMode(servoBase, PI_INPUT);
	gpioSetMode(servoTop, PI_INPUT);
	gpioTerminate();

	return 0;
}
